#define N 10

#include <stdio.h>
#include <shmem.h>

int targ_g[N];
long lsrc_g[N];
int src_g[N];

int main(void)
{
    int i, src[N], targ[N];
    long ltarg[N];
    static int starg[N];
    int nbr, lnbr;
    int my_pe, npes;

    start_pes(0);

    my_pe = shmem_my_pe();
    npes = shmem_n_pes();

    nbr = (my_pe + 1) % npes;
    lnbr = (my_pe - 1) % npes;

    for(i=0; i< N; i++) {
        src[i] = my_pe + i;
        lsrc_g[i] = my_pe + i*i;
    }

    /* non-symmetric variable */
    shmem_int_put(targ, src, N, nbr);

    shmem_int_put(targ_g, src_g, N, nbr);

    shmem_int_put(starg, src, N, nbr);

    shmem_long_get(ltarg, lsrc_g, N, nbr);

    shmem_barrier_all();  /* sync sender and receiver */

    if (my_pe == 0) {
        printf("targ[0] = %d (should be %d)\n", targ[0], lnbr);
        printf("targ_g[0] = %d (should be %d)\n", targ_g[0], lnbr);
        printf("starg[0] = %d (should be %d)\n", starg[0], lnbr);
        printf("ltarg[0] = %d (should be %d)\n", ltarg[0], lnbr);
    }

    return 0;
}

