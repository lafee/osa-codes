#define N 10
#include <shmem.h>

int main(void) {
      int i;
      int src[N];
      int targ[N];
      int *targp = targ;   
      start_pes(0);
      
      for(i=0; i< N; i++)
         src[i] = my_pe() + i;

      if (my_pe()==0) {
         shmem_int_put(targp, src, N, 1);
     }
                 
      shmem_barrier_all();  /* sync sender and receiver */
      return 1;   
             
}

