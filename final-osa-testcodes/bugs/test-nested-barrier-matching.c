/*
 * Bug: This is not working because we have two nested branches
 */

#include <stdio.h>
#include <shmem.h>

long pSync[_SHMEM_REDUCE_SYNC_SIZE];
int pWrk[_SHMEM_REDUCE_SYNC_SIZE];
 int x, y, old, i, temp;
 
int main(int argc, char *argv[]){
  int me, npes;
 
  start_pes(0);
  
  me = shmem_my_pe();
  npes = shmem_n_pes();

  for (i = 0; i < _SHMEM_REDUCE_SYNC_SIZE; i += 1)
    {
      pSync[i] = _SHMEM_SYNC_VALUE;
    }
  x = 42; 
  y = 0;
  if(me==0){
    shmem_barrier_all();
    temp = x+y;
    shmem_barrier_all();
  }
  else { 
    if(me==1){
      shmem_barrier_all();
      old = shmem_int_finc (&y, 0);
      shmem_int_sum_to_all(&y,&x, 
			   1,1,0,npes-1,pWrk,pSync);
      x= x+10;
      shmem_int_get(&y,&y,1,0);
      shmem_barrier_all();
    }
    else{
      shmem_barrier_all();
      shmem_int_sum_to_all(&y,&x,
			   1,1,0,npes-1,pWrk,pSync);
      x=y*0.23;
      shmem_barrier_all();
    }
  }
  if (me == 0) {
    printf("value in temp is %d (should be 42)\n", temp);
  }

  return 0;
}
