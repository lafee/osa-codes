
#include<stdio.h>
#include<shmem.h>
#include <stdlib.h>
int **z;
int main()
{
  int n;
  int *x;
  int *y;
 
 
  printf("Enter num: ");
  scanf("%d",&n);
   
  y = (int*) shmalloc(sizeof(int)); // memory allocated on symmetric heap
  x = (int*) malloc(sizeof(int));   // memory allocated on non symmetric heap

 
  if((n%2)==0) 
     z = &y;
  else
     z = &x;

  **z +=1; 
  //2nd param - y- is referencing to a symmetric location
  shmem_int_put(y,x,2,2);

  printf("%d", y); 
  return 1;
}
