#include <stdio.h>
#include <shmem.h>

//int me, npes;

int main()
{
  int me = shmem_my_pe();
  int npes = shmem_n_pes();
  int a;
  start_pes(0);
  
  shmem_barrier_all();
  
  if(me%2==0){
    a = 42;
    shmem_barrier_all();	
  }	
  else{	
    a = 0;
    //shmem_barrier_all();	
  }	
  
  shmem_barrier_all();

  if (me == 0) {
    printf("value in a is %d (should be 42)\n", a);
  }

  return 0;
}
