#include <stdio.h>
#include <shmem.h>


int main(int argc, char *argv[]){
  int me, npes;
  int x, y, old;
  me = shmem_my_pe();
  npes = shmem_n_pes();
  start_pes(0);
  if(me==0){
    shmem_barrier_all();
    int temp = x+y;
    shmem_barrier_all();
  }
  else { 
    if(me==1){
      shmem_barrier_all();
      old = shmem_int_finc (&y, 0);
      shmem_int_sum_to_all(&y,&x, 
			   1,1,0,npes-1,pWrk,pSync);
      x= x+10;
      shmem_int_get(&y,&y,1,0);
      shmem_barrier_all();
    }
    else{
      shmem_barrier_all();
      shmem_int_sum_to_all(&y,&x,
			   1,1,0,npes-1,pWrk,pSync);
      x=y*0.23
	shmem_barrier_all();
    }
  }
  if (mype == 0) {
    printf("value in a is %d (should be 42)\n", a);
  }

  return 0;
}
