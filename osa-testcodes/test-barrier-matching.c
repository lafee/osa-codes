#include <stdio.h>
#include <shmem.h>

int mype, npes;
int a, b;

int main()
{
    mype = shmem_my_pe();
    npes = shmem_n_pes();
    start_pes(0);

    shmem_barrier_all();

    if(me%2==0){
      a = 42;
      shmem_barrier_all();	
    }	
    else{	
      a = 0;
      shmem_barrier_all();	
    }	
 
    shmem_barrier_all();

    if (mype == 0) {
        printf("value in a is %d (should be 42)\n", a);
    }

    return 0;
}
