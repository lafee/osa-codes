#define N 10
#include <shmem.h>
int targg[N];
int main(void) {
      int i;
      int src[N];
      int targ[N];
      static int targ_static[N];
         
//      start_pes(0);  
      
      for(i=0; i< N; i++)
         src[i] = my_pe() + i;

      shmem_int_put(targ_static, src, N, 3);

      shmem_int_get(targ, targ_static, N, 3);
                 
      shmem_barrier_all();  /* sync sender and receiver */
      shmem_finalize();
      return 1;   
             
}

