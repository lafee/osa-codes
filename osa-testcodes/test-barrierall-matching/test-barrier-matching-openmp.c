#include <stdio.h>
#include <omp.h>

int me, npes;
int a;

int main()
{
#pragma omp parallel num_threads(4) private(me, npes,a)
{
  me = omp_get_thread_num();	
  npes = omp_get_num_threads();
  if(me%2==0){
#pragma omp barrier
    a = 42;
#pragma omp barrier	
  }	
  else{	
#pragma omp barrier
    a = 0;
#pragma omp barrier	
  }	
       
  //    shmem_barrier_all();
    
  if (me%2 == 0) {
    printf("value in a is %d (should be 42), me is %d, and npes is  %d\n", a, me, npes);
  }
  else
     printf("value in a is %d (should be 0), me is %d, and npes is  %d\n", a, me, npes);
   
 
}   
  return 0;
}
