#include <shmem.h>
#include <stdlib.h>
int n1=101;

int main(int argc, char *argv[]) {
  int n = 100;
  // int n1=101;
  start_pes(0);
  int nn = (n-1) / _num_pes();
  int n_local0 = 1 + _my_pe() * nn;
  int n_local1 = 1 + (_my_pe()+1) * nn;
  // allocate only local part + ghost zone of the arrays x,y
  float *x, *y;

  
  x = (float*) malloc((n_local1 - n_local0 + 2)*sizeof(float));
  y = (float*) malloc((n_local1 - n_local0 + 2)*sizeof(float));  // forgot shmalloc

  shmem_barrier_all();

  //... // fill x, y

  // fill ghost zone
  if (_my_pe() > 0)
   shmem_float_get(x,y,n1,1); // extra code
   shmem_float_put(y,x, 1, _my_pe()-1);

   shmem_barrier_all();

  // do computation
  float e = 0;
  int i;
  for (i=n_local0; i<n_local1; ++i) {
   x[i] += ( y[i+1] + y[i-1] )*.5;
   e += y[i] * y[i];
  }

  static float work[_SHMEM_REDUCE_SYNC_SIZE];
  static long sync[_SHMEM_REDUCE_SYNC_SIZE];
  static float el, es;
  el = e;
  shmem_float_sum_to_all(&es, &el, 1,
   0, 0, _num_pes(), work, sync);
  e = es;

 // ... // output x, e

  x += (n_local0 - 1);
  y += (n_local0 - 1);
  shfree(x);
  shfree(y);
  return 0;
}
