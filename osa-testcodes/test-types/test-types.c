#define N 10
#include <shmem.h>
int targg[N];
long lsrcg[N];
int srcg[N]; 

int main(void) {
  int i, src[N], targ[N];
  long ltarg[N]; 
  static int targ_static[N];
  int  nbr;     
  int my_pe, npes;
   
  start_pes(0);
  
  my_pe = shmem_my_pe();
  npes = shmem_n_pes();

  nbr = (my_pe + 1) % npes;

  for(i=0; i< N; i++) {
    src[i] = my_pe + i;
    lsrcg[i] = my_pe + i*i;
  }

  shmem_put32(targg, srcg, N, nbr);    
  shmem_get64(ltarg, lsrcg, N, nbr);
  
  shmem_get32(ltarg, lsrcg, N, nbr);
  shmem_put64(targg, srcg, N, nbr);
                      
  shmem_barrier_all();  /* sync sender and receiver */
  return 1;   
}

