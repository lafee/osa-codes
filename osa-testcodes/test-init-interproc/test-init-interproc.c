#include <stdio.h>
#include <shmem.h>

void foo();

int mype, npes;
int *a, b;

int main()
{
    mype = shmem_my_pe();
    npes = shmem_n_pes();
    start_pes(0);

    shmem_barrier_all();

    /* warning generated without the following shmalloc for a */
    //a = shmalloc(sizeof *a);

    foo();

    shmem_barrier_all();

    if (mype == 0) {
        printf("value in a is %d (should be 100)\n", *a);
    }

    return 0;
}

void foo()
{
    if (mype == 1) {
        b = 100;
        shmem_putmem(a, &b, 1, 0);
    }
}
