          #include <shmem.h>
          main()
          {
             short source[10] = { 1, 2, 3, 4, 5,
                                  6, 7, 8, 9, 10 };
             static short target[10];
             start_pes(2);
             if (_my_pe() == 0) {
                /* put 10 words into target on PE 1 */
                shmem_short_iput(target, source, 2, 1, 10, 1);
             }
             shmem_barrier_all();   /* sync sender and receiver */
             if (_my_pe() == 1) {
                shmem_udcflush();
             }
             shmem_barrier_all();   /* sync before exiting */
          }
