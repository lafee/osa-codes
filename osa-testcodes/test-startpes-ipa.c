#include <stdio.h>
#include <shmem.h>

void swapnum(int i, int j){

  int temp = i;
  i = j;
  j = temp;
//  start_pes(1);
  shmem_finalize();
}

void sub() {
  int i,j;
 // swapnum(i,j);
//  start_pes(1);
  swapnum(i,j);
}

int main(void) {
  int a = 10;
  int b = 20;

//  shmem_init();  
//  swapnum(a, b);
  sub();
//  shmem_init();
  printf("A is %d and B is %d\n", a, b);

//  shmem_init();
//  shmem_finalize();  
 
  return 0;

}

