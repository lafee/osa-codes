
#include <shmem.h>
#define N 16
int targg[N];
long froml[N];
int srcg[N]; 

int main(void) {
      int i, src[N], targ[N], len=N;
      long lget[N]; 
      static int targ_static[N];
         
      start_pes(0);
      
      for(i=0; i< N; i++) {
         src[i] = my_pe() + i;
         froml[i] = my_pe() + i*i;
        len++;    
     }

     len++;
   
      shmem_int_put(targg, srcg, len, 2);    
      shmem_int_put(targ_static, src, len, 3);
      shmem_long_get(lget,froml,len,4);
                      
      shmem_barrier_all();  /* sync sender and receiver */
      return 1;   
             
}

